*** Settings ***
Documentation   Tests to verify Swagger Pet works correctly
Library  OperatingSystem
Library  Collections
Library  /home/homza/PycharmProjects/pythonProject/ROBOT_TESTS/Requsts_learning/func_requests/requests_pet.py
Library  requests_pet.Pet  WITH NAME  Pet
Suite Setup     Log    Preparing...Start tests
Suite Teardown  Log    Cleaning...Finish tests

*** Variables ***

*** Test Cases ***

Create Pet
    [Documentation]  Creating new pet
    ${id}  Get Variable From Yaml  id
    ${name}  Get Variable From Yaml  name1
    ${json}  Pet.payload  ${id}  ${name}

    ${method}  Get Variable From Yaml  post
    ${response}  Pet.my_request  ${method}  json=${json}
    ${my_id}  Convert To String  ${response.json()["id"]}
    should be equal  ${my_id}   1314



#    ${response}  Create Pet    ${base_url}
#    ${status code}=  Convert To String    ${response.status_code}
#    should be equal  ${status code}  200
#    ${response_body}=   Convert To String    ${response.content}
#
#    ${body}=    Convert To String    ${response.content}
#    Should Contain    ${body}    Alpha
