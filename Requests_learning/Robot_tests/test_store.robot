*** Settings ***
Documentation   Tests to verify Swagger Store works correctly
Library  /home/homza/PycharmProjects/pythonProject/ROBOT_TESTS/Requests_learning/func_requests/requests_store.py
Library    Collections
Suite Setup     Log    Preparing...Start tests
Suite Teardown  Log    Cleaning...Finish tests

*** Variables ***

*** Test Cases ***

Test Get Store Map
    ${response}  Get Store Map
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200

Test Post Oder
    ${response}  Post Oder
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200

Test Get Oder By Id
    ${response}  Get Oder By Id
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200

Test Delete Oder By Id
    ${response}  Delete Oder By Id
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200

#Test Create Pet
#    [Tags]      Pet
#    ${response}=  Create Pet    ${base_url}
#    ${status code}=  Convert To String    ${response.status_code}
#    should be equal  ${status code}  200
#    ${response_body}=   Convert To String    ${response.content}
#
#    ${body}=    Convert To String    ${response.content}
#    Should Contain    ${body}    Alpha