*** Settings ***
Documentation   Tests to verify Swagger Pet works correctly
Library  /home/homza/PycharmProjects/pythonProject/ROBOT_TESTS/Requsts_learning/func_requests/requests_user.py
Library    Collections
Suite Setup     Log    Preparing...Start tests
Suite Teardown  Log    Cleaning...Finish tests

*** Test Cases ***

Test Post Create User With List
    ${response}  post_create_user_with_list
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200

    ${body}=    Convert To String    ${response.json()["message"]}
    Should Contain    ${body}    ok

Test Get User By Username
    Get User By Username
    ${response}  Get User By Username
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200


    ${body}=    Convert To String    ${response.json()["username"]}
    Should Contain    ${body}    homza


Test Put Update User
    ${response}  Put Update User
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200

    ${changed password}  Convert To String  ${response.json()["message"]}
    Should Be Equal    ${changed password}    7

Test Delete User
    ${response}  Delete User
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200

    ${body}=    Convert To String    ${response.json()["message"]}
    Should Contain    ${body}    homza

Test Get Logs User
    ${response}  Get Login User
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200

    ${body}=    Convert To String    ${response.json()["message"]}
    Should Contain    ${body}    logged in user

Test Get Logout User
    ${response}  Get Logout
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200

    ${body}=    Convert To String    ${response.json()["message"]}
    Should Contain    ${body}    ok

Test Post Create With Array
    ${response}  Post Create With Array
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200

    ${body}=    Convert To String    ${response.json()["message"]}
    Should Contain    ${body}    ok

Test Post By Logged User

    ${response}  Post By Logged User
    ${status code}=  Convert To String    ${response.status_code}
    should be equal  ${status code}  200

    ${body}=    Convert To String    ${response.json()["message"]}
    Should Contain    ${body}    5