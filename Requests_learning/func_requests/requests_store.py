import requests

data_to_oder = {
  "id": 3,
  "petId": 25,
  "quantity": 0,
  "shipDate": "2021-05-21T06:18:14.065Z",
  "status": "placed",
  "complete": "True"
}


def get_store_map():
    r = requests.get("https://petstore.swagger.io/v2/store/inventory")
    return r


def post_oder():
    r = requests.post("https://petstore.swagger.io/v2/store/order", json=data_to_oder)
    return r


def get_oder_by_id():
    r = requests.get("https://petstore.swagger.io/v2/store/order/3")
    return r


def delete_oder_by_id():
    r = requests.delete("https://petstore.swagger.io/v2/store/order/3")
    return r


if __name__ == "__main__":
    resp_get_store_map = get_store_map()
    print(resp_get_store_map.status_code)
    print(resp_get_store_map.json())
    resp_post_oder = post_oder()
    print(resp_post_oder.status_code)
    resp_get_oder_by_id = get_oder_by_id()
    print(resp_get_oder_by_id.status_code)
    resp_delete_oder_by_id = delete_oder_by_id()
    print(resp_delete_oder_by_id.status_code)
