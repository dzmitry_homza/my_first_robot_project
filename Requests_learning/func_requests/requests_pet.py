import requests
import logging
import yaml


def get_variable_from_yaml(key):
    with open("/home/homza/PycharmProjects/my_first_robot_project/Requsts_learning/func_requests/conf.yaml", "r") as f:
        v = yaml.safe_load(f)
        return v[key]


class Pet:
    def __init__(self):
        self.base_url = "https://petstore.swagger.io/v2/pet"

    @staticmethod
    def payload(id, name):
        payload = {
            "id": id,
            "category": {
                "id": 0,
                "name": "string"
            },
            "name": name,
            "photoUrls": [
                "string"
            ],
            "tags": [
                {
                    "id": 0,
                    "name": "string"
                }
            ],
            "status": "available"
        }
        return payload

    def my_request(self, method, id="", json=None, data=None, params=None):
        url = f"{self.base_url}/{id}"
        logging.info(f"REQUEST URL: {url}")
        logging.info(f"NAME OF METHOD: {method}")
        logging.info(f"BODY: {json}")
        logging.info(f"DATA: {data}")
        try:
            response = requests.request(method=method, url=url, json=json, data=data, params=params)
            logging.info(f"STATUS CODE: {response.status_code}")
            logging.info(f"RESPONSE HEADERS: {response.headers}")
            logging.info(f"RESPONSE: {response.text}")
            assert response.status_code == 200
            return response
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)


if __name__ == "__main__":
    dog = Pet()

    json = dog.payload(1314, "Alpha")
    response = dog.my_request("post", json=json)
    print("post(creating pet)", response.status_code, response.json())
    print(response.json()["id"])

    json = dog.payload(1314, "Boba")
    response = dog.my_request("put", json=json)
    print("put(updating pet)", response.status_code, response.json())

    params = get_variable_from_yaml("status")
    id = get_variable_from_yaml("by_status_url")
    response = dog.my_request("get", id="findByStatus", params=params)
    print("get(getting pets by status)", response.status_code, response.json())

    json = dog.payload(1314, "Boba")
    response = dog.my_request("get", id="1314")
    print("get(getting pet)", response.status_code, response.json())

    response = dog.my_request("delete", id="1314")
    print("delete(deleting pet)", response.status_code, response.json())





