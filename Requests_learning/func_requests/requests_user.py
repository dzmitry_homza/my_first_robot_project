import requests

user_list = [
  {
    "id": 7,
    "username": "homza",
    "firstName": "Dima",
    "lastName": "Homza",
    "email": "homza@dima",
    "password": "123",
    "phone": "6991331",
    "userStatus": 0
  }
]
user_list_chanched = {
    "id": 7,
    "username": "homza",
    "firstName": "Dima",
    "lastName": "Homza",
    "email": "homza@dima",
    "password": "999",
    "phone": "6991331",
    "userStatus": 0
  }
list_of_user_object = [
  {
    "id": 2,
    "username": "homdmz",
    "firstName": "Kolia",
    "lastName": "Noname",
    "email": "string",
    "password": "string",
    "phone": "string",
    "userStatus": 0
  }
]
list_for_logged_user = {
  "id": 5,
  "username": "string",
  "firstName": "string",
  "lastName": "string",
  "email": "string",
  "password": "string",
  "phone": "string",
  "userStatus": 0
}


def post_create_user_with_list():
    r = requests.post('https://petstore.swagger.io/v2/user/createWithList', json=user_list)
    return r


def get_user_by_username():
    r = requests.get('https://petstore.swagger.io/v2/user/homza')
    return r


def put_update_user():
    r = requests.put('https://petstore.swagger.io/v2/user/homza', json=user_list_chanched)
    return r


def delete_user():
    r = requests.delete('https://petstore.swagger.io/v2/user/homza')
    return r


def get_login_user():
    r = requests.get("https://petstore.swagger.io/v2/user/login?username=homza&password=999")
    return r


def get_logout():
    r = requests.get('https://petstore.swagger.io/v2/user/logout')
    return r


def post_create_with_array():
    r = requests.post('https://petstore.swagger.io/v2/user/createWithArray', json=list_of_user_object)
    return r


def post_by_logged_user():
    r = requests.post("https://petstore.swagger.io/v2/user", json=list_for_logged_user)
    return r


if __name__ == "__main__":
    resp_post_create_with_list = post_create_user_with_list()
    print("resp_post_create_with_list", resp_post_create_with_list.status_code)
    print(resp_post_create_with_list.json())
    resp_get_user_by_username = get_user_by_username()
    print('get_user_by_username', resp_get_user_by_username.status_code)
    print(resp_get_user_by_username.json()["username"])
    print(resp_get_user_by_username.json())
    resp_put_update_user = put_update_user()
    print(resp_put_update_user.json())
    print("Get after put-update")
    resp_get_user_by_username = get_user_by_username()
    print(resp_get_user_by_username.json())
    print(resp_put_update_user.status_code)
    resp_delete_user = delete_user()
    print(resp_delete_user.status_code)
    resp_get_logs_user = get_login_user()
    print('resp_get_logs_user', resp_get_logs_user.json())
    resp_get_logout = get_logout()
    print(resp_get_logout.status_code)
    print(resp_get_logs_user.json())
    resp_post_create_with_array = post_create_with_array()
    print(resp_post_create_with_array.status_code)
    print(resp_post_create_with_array.json())
    # resp_post_by_logged_user = post_by_logged_user()
    # print(resp_post_by_logged_user.status_code)
    # print(resp_post_by_logged_user.json())
